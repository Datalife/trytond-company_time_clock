# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import pytz
import dateutil
from collections import defaultdict
from trytond.model import (Workflow, ModelView, ModelSQL, fields, Unique,
    DeactivableMixin)
from trytond.report import Report
from trytond.pyson import Eval, If, Not, Bool, Id
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from itertools import groupby
from trytond.wizard import (Wizard, StateTransition, StateView,
    Button, StateReport)
from trytond.exceptions import UserError
from trytond.i18n import gettext
from datetime import datetime, timedelta
from functools import partial
from dateutil.relativedelta import relativedelta
from dateutil.tz import tz as dateutil_tz

TIMEZONES = [(x, x) for x in pytz.common_timezones]

TIMEZONES += [(None, '')]


class TimeClockMixin(object):
    __slots__ = ()

    @fields.depends('code')
    def get_clock_code(self):
        return self.code

    def convert_time_tz(self, time):
        szone = dateutil_tz.tzutc()

        company = self.employee and self.employee.company or None
        if company is None:
            lzone = szone
        else:
            tz = company.timezone
            lzone = dateutil_tz.gettz(tz) if tz else szone

        time_value = time.replace(tzinfo=szone).astimezone(
            lzone).replace(tzinfo=None)

        return time_value

    @classmethod
    def get_time_tz(cls, records, name=None):
        res = {}
        for record in records:
            res[record.id] = record.convert_time_tz(record.time_)
        return res

    @fields.depends('company', methods=['get_clock_code'])
    def _rounding_factor(self):
        pool = Pool()
        TimeclockCode = pool.get('company.time_clock.code')

        clock_codes = TimeclockCode.search([
            ('code', '=', self.get_clock_code())])
        clock_type = clock_codes[0].type_ if clock_codes else ''
        if not clock_codes:
            clock_type = ''
        if clock_type == 'input':
            rounding_type = self.company.input_rounding_type
        elif clock_type == 'output':
            rounding_type = self.company.output_rounding_type
        else:
            rounding_type = 'nearest'
        factors = {
            'nearest': .5,
            'upward': .0,
            'downward': 1.0,
            'no_rounding': None
        }
        return factors[rounding_type]

    def _rounded_minutes(self, minutes):
        secs = minutes * 60
        late_rounding = (self.company.grace_period or timedelta())
        rounding_factor = self._rounding_factor()
        rounding = self.company.timeclock_rounding or None
        if rounding_factor is None or not rounding:
            return minutes
        value = secs // rounding.total_seconds()
        if (secs - late_rounding.total_seconds()
                >= value * rounding.total_seconds()):
            time_factor = (secs / rounding.total_seconds()) % 1
            if time_factor > rounding_factor:
                value += 1
        return int((value * rounding.total_seconds()) / 60)

    @fields.depends('time_', 'time_tz', 'company',
        methods=['_rounding_factor', '_rounded_minutes'])
    def get_rounded_time(self, tz=False):
        time_value = self.time_
        if tz:
            time_value = self.time_tz
        if not self.company:
            return time_value
        minute = self._rounded_minutes(time_value.minute)
        if minute >= 60:
            minute = 0
            time_value += timedelta(hours=1)

        return time_value.replace(minute=minute, second=0,
            microsecond=0)

    @classmethod
    def create(cls, vlist):
        lines = super().create(vlist)
        cls.check_period_closed(lines)
        return lines

    @classmethod
    def write(cls, *args):
        super().write(*args)
        actions = iter(args)
        for lines, values in zip(actions, actions):
            cls.check_period_closed(lines)

    @classmethod
    def check_period_closed(cls, lines):
        Period = Pool().get('company.time_clock.period')

        lines = cls.browse(lines)
        for company, lines in groupby(lines, lambda m: m.company):
            periods = Period.search([
                    ('state', '=', 'closed'),
                    ('company', '=', company.id) if company else (),
                    ], order=[('date_', 'DESC')], limit=1)
            if periods:
                period, = periods
                for line in lines:
                    date = line.date
                    if date and date <= period.date_:
                        raise UserError(gettext(
                            'company_time_clock.'
                            'msg_company_employee_time_clock_period_closed',
                            date=date,
                            period=period.rec_name))


class EmployeeTimeClock(TimeClockMixin, Workflow, ModelSQL, ModelView):
    '''Employee Time clocking'''
    __name__ = 'company.employee.time_clock'

    clock_number = fields.Char('Clocking number', required=True,
        states={'readonly': (Eval('state') != 'draft')},
        depends=['state'])
    employee = fields.Many2One('company.employee', 'Employee',
        select=True, ondelete='RESTRICT',
        domain=[
            If(Bool(Eval('clock_number')),
                ('clock_number', '=', Eval('clock_number')),
                ()
            ),
            If(Eval('date'),
                [
                    ['OR',
                        ('start_date', '=', None),
                        ('start_date', '<=', Eval('date')),
                        ],
                    ['OR',
                        ('end_date', '=', None),
                        ('end_date', '>=', Eval('date')),
                        ]
                ],
                []
            )
        ],
        states={'required': (Eval('state') == 'confirmed'),
                'readonly': (Eval('state') != 'draft')},
        depends=['clock_number', 'state', 'date'])
    code = fields.Char('Code',
        states={'readonly': (Eval('state') != 'draft'),
                'required': (Eval('state') == 'confirmed')},
        depends=['state'])
    time_ = fields.DateTime('Date Time', format='%H:%M:%S', required=True,
        states={'readonly': (Eval('state') != 'draft')},
        depends=['state'])
    time_tz = fields.Function(
        fields.DateTime('Date Time TZ', format='%H:%M:%S'),
        'get_time_tz')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancelled', 'Cancelled'),
        ('confirmed', 'Confirmed')], 'State', required=True, readonly=True)
    date = fields.Date('Date', readonly=True,
        depends=['time_tz'])
    company = fields.Function(fields.Many2One('company.company', 'Company'),
        'get_company', searcher='search_company')
    time_rounded = fields.DateTime('Date Time Rounded', format='%H:%M:%S',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(EmployeeTimeClock, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('draft', 'cancelled'),
            ('cancelled', 'draft')))
        cls._buttons.update({
            'draft': {
                'icon': If((Eval('state') == 'confirmed'),
                    'tryton-back', 'tryton-undo'),
                'invisible': Eval('state') == 'draft',
                'depends': ['state']
            },
            'confirm': {
                'icon': 'tryton-forward',
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
            },
            'cancel': {
                'icon': 'tryton-cancel',
                'invisible': (Eval('state') != 'draft'),
                'depends': ['state']
            },
            'set_employee': {
                'icon': 'tryton-ok',
                'invisible': (Eval('state') != 'draft'),
                'depends': ['state']
            },
            'set_time_clock_codes': {
                'icon': 'tryton-ok',
                'invisible': (Eval('state') != 'draft'),
                'depends': ['state']
            },
            'user_edit': {}
        })

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    @classmethod
    def get_rec_name(cls, records, name):
        pool = Pool()
        Lang = pool.get('ir.lang')

        lang = Lang.get()
        language = Transaction().language
        languages = Lang.search([('code', '=', language)])
        if not languages:
            languages = Lang.search([('code', '=', 'en_US')])
        language, = languages

        res = {}
        for record in records:
            keys = {
                'time': lang.strftime(record.time_),
                'employee': record.clock_number
            }
            value = '[%(employee)s] %(time)s'
            if record.employee:
                keys['employee'] = record.employee.rec_name
            if record.code:
                value += ' (%(code)s)'
                keys['code'] = record.code
            res[record.id] = value % keys
        return res

    def get_company(self, name=None):
        if self.employee:
            return self.employee.company.id
        return None

    @classmethod
    def search_company(cls, name, clause):
        return [('employee.%s' % clause[0], ) + tuple(clause[1:])]

    @property
    def code_name(self):
        return self.code

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def delete(cls, records):
        cls.cancel(records)
        for record in records:
            if record.state != 'cancelled':
                raise UserError(gettext(
                    'company_time_clock.'
                    'msg_company_employee_time_clock_delete_cancel',
                    clock=record.rec_name))
        super(EmployeeTimeClock, cls).delete(records)

    @fields.depends('employee', 'clock_number')
    def on_change_employee(self):
        if self.employee and (not self.clock_number or
                self.clock_number != self.employee.clock_number):
            self.clock_number = self.employee.clock_number

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        pool = Pool()
        TimeclockCode = pool.get('company.time_clock.code')

        codes = [r.code for r in TimeclockCode.search([])]
        for record in records:
            if record.code not in codes:
                raise UserError(gettext(
                    'company_time_clock.'
                    'msg_company_employee_time_clock_invalid_code',
                    clock=record.code))

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @fields.depends('clock_number', 'date')
    def on_change_clock_number(self):
        if not self.clock_number:
            return
        employees = self._get_allowed_employees()
        if len(employees) == 1:
            self.employee = employees[0]

    @fields.depends('time_', 'employee')
    def on_change_with_time_tz(self):
        if self.time_:
            return self.get_time_tz([self])[self.id]

    @fields.depends('time_', 'time_tz')
    def on_change_with_date(self, name=None):
        if self.time_tz:
            return self.time_tz.date()

    def _get_allowed_employees(self):
        pool = Pool()
        Employee = pool.get('company.employee')

        domain = [('clock_number', '=', self.clock_number)]
        if self.date:
            domain.extend([
                ['OR',
                    ('start_date', '=', None),
                    ('start_date', '<=', self.date),
                    ],
                ['OR',
                    ('end_date', '=', None),
                    ('end_date', '>=', self.date),
                    ]
                ])
        return Employee.search(domain)

    @classmethod
    def _get_tc_codes(cls, records):
        pool = Pool()
        TimeclockCode = pool.get('company.time_clock.code')

        return TimeclockCode.search([])

    @classmethod
    @ModelView.button
    def set_time_clock_codes(cls, records, start_date=None,
            end_date=None, **kwargs):
        pool = Pool()
        Date = pool.get('ir.date')

        _order = [
            ('clock_number', 'ASC'),
            ('employee', 'ASC'),
            ('time_', 'ASC')
        ]
        if not records:
            company = Transaction().context.get('company', 0)
            if not end_date:
                end_date = Date.today()
            if not start_date and 'days_ago' in kwargs:
                start_date = end_date - relativedelta(
                    days=kwargs.get('days_ago', 0) or 0)
            _domain = [
                ('company', '=', company),
                ('date', '<=', end_date),
                ('state', '=', 'draft')
            ]
            if start_date:
                _domain.append(('date', '>=', start_date))
            if kwargs.get('blank_test', False):
                _domain.append(('code', 'in', ('', None)))

            records = cls.search(_domain, order=_order)
        else:
            records = cls.search([
                ('id', 'in', list(map(int, records)))], order=_order)

        tc_codes = cls._get_tc_codes(records)
        tc_codes.sort(key=lambda x: x.type_)
        tc_codes = {x: [z.code for z in y] for x, y in groupby(
            tc_codes,
            key=lambda x: x.type_)}
        changes = {
            0: [],  # inputs
            1: []  # outputs
        }
        for key_employee, grouped_tc in groupby(records,
                key=lambda x: x.clock_number):
            for key_time_, subgrouped_tc in groupby(grouped_tc,
                    key=lambda x: x.date):
                subgrouped_tc = list(subgrouped_tc)
                for ii, item in enumerate(subgrouped_tc):
                    if (item.code and item.code not in tc_codes['input'] and
                            item.code not in tc_codes['output']):
                        continue
                    _code = (tc_codes['output'][0] if ii % 2
                        else tc_codes['input'][0])

                    if item.code != _code:
                        changes[ii % 2].append(item)

        if changes[0]:
            cls.write(changes[0], {'code': tc_codes['input'][0]})
        if changes[1]:
            cls.write(changes[1], {'code': tc_codes['output'][0]})

    @classmethod
    @ModelView.button
    def set_employee(cls, records):
        for record in records:
            record.on_change_clock_number()
        cls.save(records)

    @classmethod
    def get_input_output_moves(cls, records):
        pool = Pool()
        Code = pool.get('company.time_clock.code')

        employees = set([r.employee.id
            if r.employee else None for r in records])
        assert len(employees) == 1

        codes = Code.search([('type_', '=', 'input')])
        code_types = {}
        for code in codes:
            code_types.setdefault(code.type_, []).append(code.code)

        _in = [i for i in records if i.code in code_types['input']]
        _others = [i for i in records if i.code not in code_types['input']]
        return _in, _others

    @classmethod
    def get_employee_hours(cls, records, rounded=False):
        _in, _others = cls.get_input_output_moves(records)
        res = timedelta()
        for i, other in enumerate(_others):
            if len(_in) > i:
                if not rounded:
                    res += (other.time_ - _in[i].time_)
                else:
                    res += (other.get_rounded_time() -
                        _in[i].get_rounded_time())
        return res

    @classmethod
    @ModelView.button_action('company_time_clock.wizard_time_clock_user_edit')
    def user_edit(cls, records):
        pass

    @classmethod
    def export_data(cls, records, fields_names):
        pool = Pool()
        Company = pool.get('company.company')
        res = super(EmployeeTimeClock, cls).export_data(records, fields_names)

        company = Company(Transaction().context['company'])
        lzone = (dateutil.tz.gettz(company.timezone)
            if company.timezone else dateutil.tz.tzutc())
        szone = dateutil.tz.tzutc()

        for item in res:
            for _id_field, _field in enumerate(item):
                if isinstance(_field, datetime):
                    item[_id_field] = _field.replace(
                        tzinfo=szone).astimezone(lzone).replace(tzinfo=None)

        return res

    @classmethod
    def create(cls, vlist):
        lines = super().create(vlist)
        to_write_date = defaultdict(list)
        to_write_round = defaultdict(list)
        for line in lines:
            date = line.on_change_with_date()
            time_rounded = line.get_rounded_time()
            if line.date != date:
                to_write_date[date].append(line)
            if time_rounded and line.time_rounded != time_rounded:
                to_write_round[time_rounded].append(line)
        args = []
        if to_write_date:
            for date, records in to_write_date.items():
                args.extend([records, {'date': date}])
        if to_write_round:
            for time_rounded, records in to_write_round.items():
                args.extend([records, {'time_rounded': time_rounded}])
        if args:
            cls.write(*args)
        return lines

    @classmethod
    def write(cls, *args):
        super().write(*args)

        to_write_date = defaultdict(list)
        to_write_round = defaultdict(list)
        actions = iter(args)
        for lines, values in zip(actions, actions):
            for line in lines:
                date = line.on_change_with_date()
                time_rounded = None
                if set(values).intersection({'employee', 'code', 'time_'}):
                    time_rounded = line.get_rounded_time()
                    if line.time_rounded != time_rounded:
                        to_write_round[time_rounded].append(line)
                if line.date != date:
                    to_write_date[date].append(line)
        args = []
        if to_write_date:
            for date, records in to_write_date.items():
                args.extend([records, {'date': date}])
        if to_write_round:
            for time_rounded, records in to_write_round.items():
                args.extend([records, {'time_rounded': time_rounded}])
        if args:
            cls.write(*args)

    @classmethod
    def cron_confirm(cls):
        company = Transaction().context['company']
        records = cls.search([
            ('state', '=', 'draft'),
            ('company', '=', company)])
        if records:
            cls.confirm(records)

    @classmethod
    def cron_set_codes(cls):
        cls.set_time_clock_codes([], blank_test=True)

    def get_rounded_time_tz(self):
        if self.time_rounded:
            return self.convert_time_tz(self.time_rounded)
        return self.get_rounded_time(True)


class EmployeeTimeClockDevice(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock'

    device = fields.Many2One('device', 'Device', ondelete='RESTRICT')

    @classmethod
    def _get_tc_codes(cls, records):
        tc_codes = super()._get_tc_codes(records)

        device_types = set(
            r.device.type for r in records if r.device and r.device.type)

        if len(device_types) == 1:
            device_type, = device_types
            tc_codes = [t for t in tc_codes if device_type in t.device_types]

        return tc_codes


class TimeClockPeriod(Workflow, ModelSQL, ModelView):
    '''Time clocking Period'''
    __name__ = 'company.time_clock.period'

    company = fields.Many2One('company.company', 'Company', required=True,
            states={'readonly': Eval('state') == 'closed'}, depends=['state'])
    date_ = fields.Date('Date', required=True, states={
                    'readonly': Eval('state') == 'closed'}, depends=['state'])
    state = fields.Selection([
             ('draft', 'Draft'),
             ('closed', 'Closed')
             ], 'State', readonly=True, required=True)

    @classmethod
    def __setup__(cls):
        super(TimeClockPeriod, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'closed'),
            ('closed', 'draft'),
            ))
        cls._buttons.update({
            'draft': {
                'icon': 'tryton-back',
                'invisible': Eval('state') != 'closed',
                'depends': ['state']
                },
            'close': {
                'icon': 'tryton-forward',
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
                },
            })

    @classmethod
    def get_rec_name(cls, records, name):
        pool = Pool()
        Lang = pool.get('ir.lang')

        lang = Lang.get()
        language = Transaction().language
        languages = Lang.search([('code', '=', language)])
        if not languages:
            languages = Lang.search([('code', '=', 'en_US')])
        language, = languages

        res = {}
        for record in records:
            res[record.id] = '%s %s' % (
                record.company.rec_name,
                lang.strftime(record.date_))
        return res

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        pool = Pool()
        TimeClock = pool.get('company.employee.time_clock')
        Date = pool.get('ir.date')

        today = Date.today()

        companies = {}
        for record in records:
            companies.setdefault(record.company.id, record.date_)
            if record.date_ > companies[record.company.id]:
                companies[record.id] = record.date_
            if record.date_ >= today:
                raise UserError(gettext(
                    'company_time_clock.msg_company_time_clock_period_close_period_future_today'))

        for company_id, max_date in companies.items():
            if TimeClock.search([
                        ('state', '=', 'draft'),
                        ['OR',
                            ('employee', '=', None),
                            ('employee.company', '=', company_id)],
                        ('date', '<=', max_date)],
                        limit=1):
                raise UserError(gettext(
                    'company_time_clock.msg_company_time_clock_period_close_period_draft_time_clock'))

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class TimeClockPrintStart(ModelView):
    '''Start printing Employee Time cloking'''
    __name__ = 'company.employee.time_clock.print_start'

    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company')
    employee = fields.Many2One('company.employee', 'Employee',
        domain=[If(~Id('company_time_clock', 'group_clocktime_admin').in_(
                Eval('context', {}).get('groups', [])),
            ('id', 'in', Eval('employees')),
            ())],
        depends=['employees'])
    employees = fields.Many2Many('company.employee', None, None, 'Employees')

    @staticmethod
    def default_company():
        pool = Pool()
        Company = pool.get('company.company')
        comp = Company.search([])
        if len(comp) == 1:
            return comp[0].id
        return None


class TimeClockPrint(Wizard):
    '''Print Employee Time clocking'''
    __name__ = 'company.employee.time_clock.print'

    start = StateView('company.employee.time_clock.print_start',
        'company_time_clock.time_clock_print_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Print', 'pre_print', 'tryton-print', default=True)])
    pre_print = StateTransition()
    print_ = StateReport('company.employee.time_clock.list')

    def default_start(self, fields):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        User = pool.get('res.user')

        res = {}
        group_id = Modeldata.get_id('company_time_clock',
            'group_clocktime_admin')
        if group_id not in Transaction().context.get('groups', []):
            user = User(Transaction().user)
            res['employees'] = list(map(int, user.employees))
        return res

    def transition_pre_print(self):
        return 'print_'

    def do_print_(self, action):
        pool = Pool()
        Clocking = pool.get('company.employee.time_clock')

        with Transaction().set_context(_check_access=True):
            res = Clocking.search(self._get_timeclock_domain(),
                order=[
                    ('employee.company', 'ASC'),
                    ('employee', 'ASC'),
                    ('date', 'ASC'),
                    ('time_', 'ASC')])
        return action, {
            'ids': [r.id for r in res],
            }

    def _get_timeclock_domain(self):
        _domain = [
            ('date', '>=', self.start.start_date),
            ('date', '<=', self.start.end_date)]
        if self.start.company:
            _domain.append(('company', '=', self.start.company.id))
        if getattr(self.start, 'employee', None):
            _domain.append(('employee', '=', self.start.employee.id))
        return _domain


class EmployeeTimeClockReport(object):

    @classmethod
    def get_context(cls, records, header, data):

        if records:
            records = sorted(records, key=lambda x: (
                x.company and x.company.id or 0,
                x.employee and x.employee.id or 0,
                x.date,
                x.time_tz))

        report_context = super().get_context(records, header, data)

        company_days = {}
        for record in records:
            company_days.setdefault(
                record.company and record.company.id or None, set()).add(
                record.date)
        report_context['dates'] = company_days
        report_context['grouped_data'] = lambda items, group_key: \
            cls.grouped_data(items, group_key)
        report_context['employee_hours'] = lambda items, decimal=False, \
            rounded=False: cls.employee_hours(items, decimal, rounded)
        report_context['max_records'] = cls.get_max_records(records)
        return report_context

    @classmethod
    def get_max_records(cls, records):
        max_records = 0
        for company, days in cls.grouped_data(records, 'company'):
            for employee, employees in cls.grouped_data(days, 'employee'):
                for day, moves in cls.grouped_data(employees, 'date'):
                    if len(moves) > max_records:
                        max_records = len(moves)
        return max_records

    @classmethod
    def grouped_data(cls, items, group_key):
        if not items:
            return

        def _group_items(key, item):
            keys = {
                    'company': item.company if item.company else '',
                    'employee': item.employee if item.employee else '',
                    'date': item.date
                }
            return keys[key]
        keyfunc = partial(_group_items, group_key)
        for _key, _grouped_data in groupby(items, key=keyfunc):
            yield _key, list(_grouped_data)

    @classmethod
    def employee_hours(cls, items, decimal=False, rounded=False):
        pool = Pool()
        if not items:
            return
        Model = pool.get(items[0].__name__)
        value = Model.get_employee_hours(items, rounded=rounded)
        if decimal:
            value = value.total_seconds() / 3600
        return abs(value)

    @classmethod
    def execute(cls, ids, data):
        pool = Pool()
        ActionReport = pool.get('ir.action.report')

        action_id = data.get('action_id')
        if action_id is None:
            action_reports = ActionReport.search([
                    ('report_name', '=', cls.__name__)
                    ])
            assert action_reports, '%s not found' % cls
            action_report = action_reports[0]
        else:
            action_report = ActionReport(action_id)

        return super().execute(ids, data)[:-1] + (action_report.name, )


class EmployeeTimeClockList(EmployeeTimeClockReport, Report):
    '''Employee time clocking list'''
    __name__ = 'company.employee.time_clock.list'


class TimeClockCode_Device(ModelSQL):
    'Time clocking code - Device'
    __name__ = 'company.time_clock.code-device.type'
    device_type = fields.Many2One(
        'device.type', 'Device Type', required=True, select=True)
    time_clock_code = fields.Many2One(
        'company.time_clock.code',
        'Time Clock Code',
        required=True, select=True)


class TimeClockCode(ModelSQL, ModelView, DeactivableMixin):
    '''Time clocking code'''
    __name__ = 'company.time_clock.code'
    _rec_name = 'code'

    code = fields.Char('Code', required=True)
    type_ = fields.Selection([
        ('input', 'Input'),
        ('output', 'Output'),
        ('breakfast', 'Breakfast'),
        ('medical', 'Medical'),
        ('errand', 'Errand'),
        ('personal', 'Personal'),
        ('other', 'Other')], 'Type', required=True)

    @classmethod
    def __setup__(cls):
        super(TimeClockCode, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('code_uniq', Unique(t, t.code),
                'company_time_clock.msg_company_time_clock_code_code_uniq'),
        ]


class TimeClockCodeDevice(metaclass=PoolMeta):
    __name__ = 'company.time_clock.code'

    device_types = fields.Many2Many(
        'company.time_clock.code-device.type',
        'time_clock_code',
        'device_type',
        'Devices')


class TimeClockUserEditData(ModelView):
    """Time clocking user edit data"""
    __name__ = 'company.employee.time_clock.user_edit.data'

    timeclock = fields.Many2One('company.employee.time_clock', 'Time clock',
        readonly=True)
    employee = fields.Many2One('company.employee', 'Employee',
        required=True, domain=[
            ('id', 'in', Eval('employees', []))])
    employees = fields.One2Many('company.employee', None, 'Employees')
    time_ = fields.DateTime('Date Time', format='%H:%M:%S', required=True)
    code = fields.Many2One('company.time_clock.code', 'Code')
    must_delete = fields.Boolean('Must delete', readonly=True)

    @fields.depends('code', 'employee', 'timeclock',
     'employee', methods=['_get_code'])
    def on_change_employee(self):
        if self.employee:
            if not self.code and not self.timeclock:
                self.code = self._get_code()

    @fields.depends('code', 'timeclock', 'employee', methods=['_get_code'])
    def on_change_time_(self):
        if self.employee and not self.code and not self.timeclock:
            self.code = self._get_code()

    @fields.depends('employee', 'time_')
    def _get_code(self):
        pool = Pool()
        TimeClock = pool.get('company.employee.time_clock')
        TimeClockCode = pool.get('company.time_clock.code')

        if self.time_:
            timeclocking = TimeClock.search([
                ('employee', '=', self.employee.id),
                ('time_', '<', self.time_)
                ], limit=1, order=[('time_', 'DESC')])
            if timeclocking:
                timeclocking, = timeclocking
                if not timeclocking.code:
                    return None

                type_clock = TimeClockCode.search([
                        ('code', '=', timeclocking.code)
                        ], limit=1)
                reverse = {
                    'input': 'output',
                    'output': 'input'
                }
                if not type_clock or type_clock[0].type_ not in reverse:
                    return None
                type_clock, = type_clock
                type_ = reverse[type_clock.type_]
                type_clock = TimeClockCode.search([
                        ('type_', '=', type_)
                    ], limit=1)
                if type_clock:
                    return type_clock[0]


class TimeClockUserEditConfirm(ModelView):
    """Time clocking user edit confirmation"""
    __name__ = 'company.employee.time_clock.user_edit.confirm'

    password = fields.Char('Password', required=True)


class TimeClockUserEdit(Wizard):
    '''Time clocking user edit'''
    __name__ = 'company.employee.time_clock.user_edit'

    start = StateTransition()
    data = StateView('company.employee.time_clock.user_edit.data',
        'company_time_clock.time_clock_user_edit_data_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Delete', 'delete_', 'tryton-delete', states={
            'invisible': Not(Bool(Eval('timeclock', None)))}),
         Button('OK', 'confirm_', 'tryton-ok', default=True)])
    confirm_ = StateView('company.employee.time_clock.user_edit.confirm',
        'company_time_clock.time_clock_user_edit_confirm_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'finish_', 'tryton-ok', default=True)])
    delete_ = StateTransition()
    finish_ = StateTransition()

    def transition_start(self):
        pool = Pool()
        User = pool.get('res.user')
        Timeclock = pool.get('company.employee.time_clock')

        if not Transaction().context['active_id']:
            return 'data'
        if Transaction().context['active_model'] == Timeclock.__name__:
            timeclocking = Timeclock(Transaction().context['active_id'])
            if timeclocking.state != 'draft':
                raise UserError(gettext(
                    'company_time_clock.msg_company_employee_time_clock_user_edit_wrong_state'))
            user_id = Transaction().user
            if user_id == 0:
                user_id = Transaction().context.get('user', user_id)
            employees = User(user_id).employees
            if (not timeclocking.employee or
                    timeclocking.employee not in employees):
                raise UserError(gettext(
                    'company_time_clock.msg_company_employee_time_clock_user_edit_wrong_employee',
                    employee=timeclocking.employee.rec_name))
        return 'data'

    def default_data(self, fields):
        pool = Pool()
        User = pool.get('res.user')
        Timeclock = pool.get('company.employee.time_clock')

        user_id = Transaction().user
        if user_id == 0:
            user_id = Transaction().context.get('user', user_id)
        employees = User(user_id).employees
        res = {
            'employees': list(map(int, employees))
        }
        if Transaction().context['active_model'] == Timeclock.__name__:
            record = Timeclock(Transaction().context['active_id'])
            res['timeclock'] = record.id
            res['employee'] = record.employee.id
            res['time_'] = record.time_
        else:
            if len(employees) == 1:
                res['employee'] = employees[0].id
            res['time_'] = datetime.now()
        return res

    def transition_delete_(self):
        self.data.must_delete = True
        return 'confirm_'

    def transition_finish_(self):
        pool = Pool()
        User = pool.get('res.user')
        Timeclock = pool.get('company.employee.time_clock')

        # check password
        user_id = Transaction().user
        if user_id == 0:
            user_id = Transaction().context.get('user', user_id)
        user = User(user_id)
        _, password_hash, _ = User._get_login(user.login)
        if password_hash:
            valid, _ = User.check_password(self.confirm_.password,
                password_hash)
            if not valid:
                raise UserError(gettext('company_time_clock.'
                    'msg_company_employee_time_clock_user_edit_wrong_pwd'))

        with Transaction().set_context(_check_access=False):
            if self.data.must_delete:
                Timeclock.delete([getattr(self.data, 'timeclock')])
            else:
                res = getattr(self.data, 'timeclock', None) or Timeclock()
                res.employee = self.data.employee
                res.time_ = self.data.time_
                res.code = self.data.code.code
                res.clock_number = res.employee.clock_number
                res.save()

        return 'end'

    def end(self):
        return 'reload'
