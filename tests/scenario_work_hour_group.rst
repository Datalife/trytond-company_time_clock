========================
Work Hour Group Scenario
========================

Imports::

    >>> from datetime import time, timedelta, date
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from dateutil.relativedelta import relativedelta
    >>> today = date.today()


Install company_time_clock::

    >>> config = activate_modules('company_time_clock')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Work hour::

    >>> WorkHour = Model.get('company.work_hour')
    >>> workhour = WorkHour()
    >>> workhour.name = 'Work hour 1'
    >>> workhour.timezone = 'Europe/Madrid'
    >>> workhour.save()


Other work hour::

    >>> workhour2 = WorkHour()
    >>> workhour2.name = 'Work hour 2'
    >>> workhour2.timezone = 'Europe/Madrid'
    >>> workhour2.save()


Create work hour group::

    >>> Group = Model.get('company.work_hour.group')
    >>> group = Group()
    >>> group.name = 'Group 1'
    >>> line = group.lines.new()
    >>> line.work_hour = workhour
    >>> line.period = timedelta(weeks=6)
    >>> line2 = group.lines.new()
    >>> line2.work_hour = workhour2
    >>> line2.period = timedelta(weeks=2)
    >>> group.save()


Employee::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Pam Beesly')
    >>> party.save()
    >>> Employee = Model.get('company.employee')
    >>> employee = Employee(party=party, company=company)
    >>> employee.save()


Start rotate work hour Wizard::

    >>> len(employee.work_hours)
    0
    >>> create_rotate_work_hour = Wizard('company.employee.rotate_work_hour', [employee])
    >>> create_rotate_work_hour.form.start_date = today
    >>> create_rotate_work_hour.form.end_date = today + relativedelta(months=4)
    >>> create_rotate_work_hour.form.work_hour_group = group
    >>> create_rotate_work_hour.execute('rotate_')
    >>> employee.work_hours[0].start_date == today and employee.work_hours[0].work_hour == workhour
    True
    >>> employee.work_hours[1].start_date == employee.work_hours[0].start_date + relativedelta(weeks=6) and employee.work_hours[1].work_hour == workhour2
    True
    >>> employee.work_hours[2].start_date == employee.work_hours[1].start_date + relativedelta(weeks=2) and employee.work_hours[2].work_hour == workhour
    True
    >>> employee.work_hours[3].start_date == employee.work_hours[2].start_date + relativedelta(weeks=6) and employee.work_hours[3].work_hour == workhour2
    True
    >>> employee.work_hours[4].start_date == employee.work_hours[3].start_date + relativedelta(weeks=2) and employee.work_hours[4].work_hour == workhour
    True
    >>> len(employee.work_hours)
    5


Check empty period::

    >>> group.lines[-1].period = None
    >>> group.save()
    >>> create_rotate_work_hour = Wizard('company.employee.rotate_work_hour', [employee])
    >>> create_rotate_work_hour.form.start_date = employee.work_hours[4].start_date + relativedelta(days=1)
    >>> create_rotate_work_hour.form.end_date = today + relativedelta(years=2)
    >>> create_rotate_work_hour.form.work_hour_group = group
    >>> create_rotate_work_hour.execute('rotate_')
    >>> len(employee.work_hours)
    7


Create employee without work hour group::

    >>> party2 = Party(name='Party 2')
    >>> party2.save()
    >>> employee2 = Employee(party=party2, company=company)
    >>> employee2.save()


Check rotate work hour Wizard::

    >>> len(employee2.work_hours)
    0
    >>> create_rotate_work_hour = Wizard('company.employee.rotate_work_hour', [employee2])
    >>> create_rotate_work_hour.form.start_date = today
    >>> create_rotate_work_hour.form.end_date = today + relativedelta(months=4)
    >>> create_rotate_work_hour.form.work_hour_group = group
    >>> create_rotate_work_hour.execute('rotate_')
    >>> len(employee2.work_hours)
    2