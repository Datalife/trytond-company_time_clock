===========================
Company Time Clock Scenario
===========================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from datetime import time, date, timedelta
    >>> from trytond.model import ModelView, ModelSQL, fields, Unique
    >>> from dateutil.relativedelta import relativedelta


Install company_time_clock::

    >>> config = activate_modules('company_time_clock')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get today date::

    >>> import datetime
    >>> today = datetime.date.today()
	>>> now = datetime.datetime.now()


Set clocking sequence::
	>>> Sequence = Model.get('ir.sequence')
    >>> ModelData = Model.get('ir.model.data')
    >>> data_seq, = ModelData.find([
    ...     ('module', '=', 'company_time_clock'),
    ...     ('fs_id', '=', 'sequence_party_time_clock')])
    >>> seq, = Sequence.find([('id', '=', data_seq.db_id)])
	>>> Conf = Model.get('party.configuration')
	>>> conf = Conf(1)
	>>> conf.clocking_sequence = seq
	>>> conf.save()


Employee::

	>>> Party = Model.get('party.party')
	>>> party = Party(name='Pam Beesly')
    >>> party.save()
    >>> party2 = Party(name='Jim Halpert')
    >>> party2.save()
	>>> Employee = Model.get('company.employee')
	>>> employee = Employee(party=party, company=company)
	>>> employee.save()
	>>> employee.clock_number
	'1'
    >>> employee2 = Employee(party=party2, company=company)
    >>> employee2.save()
    >>> employee2.clock_number
    '2'


Create user::

    >>> User = Model.get('res.user')
    >>> admin, = User.find([('login', '=', 'admin')])
    >>> user = User()
    >>> user.name = 'User 1'
    >>> user.login = 'user1'
    >>> user.password = '12345678'
    >>> user.company = company
    >>> user.save()


Time Clock::

	>>> TimeClock = Model.get('company.employee.time_clock')
	>>> timeclock = TimeClock(clock_number='1')
	>>> timeclock.employee == employee
	True
	>>> timeclock.time_ = now + relativedelta(days=-2)
	>>> timeclock.save()
	>>> timeclock.state
	'draft'
    >>> timeclock.click('confirm')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Time clocking code "None" not defined. - 
    >>> timeclock.code = 'IN'
    >>> timeclock.save()
    >>> timeclock.click('confirm')
	>>> timeclock.state
	'confirmed'


Invalid Time Clock::

	>>> timeclock2 = TimeClock(clock_number='5')
	>>> not timeclock2.employee
	True
	>>> timeclock2.time_ = now
	>>> timeclock2.save()
	>>> timeclock2.click('confirm')
	Traceback (most recent call last):
		...
	trytond.exceptions.UserError: Time clocking code "None" not defined. - 


Work Hour::

	>>> WorkHour = Model.get('company.work_hour')
	>>> workhour = WorkHour()
	>>> workhour.name = 'Intensive working day'
    >>> workhour.timezone = 'Europe/Madrid'
	>>> workhour.save()
	>>> line = workhour.monday_lines.new()
	>>> line.type_ = 'in'
	>>> line.time_ = time(8, 30, 00)
	>>> line = workhour.monday_lines.new()
	>>> line.type_ = 'out'
	>>> line.time_ = time(15, 00, 00)
	>>> workhour.save()


Period::

    >>> timeclock.click('draft')
	>>> Period = Model.get('company.time_clock.period')
	>>> period = Period()
	>>> period.company = company
	>>> period.date_ = now + relativedelta(days=-1)
	>>> period.click('close')
	Traceback (most recent call last):
		...
	trytond.exceptions.UserError: You can not close a period when there still are draft time clocking records. - 


Time Clock Rounded::

    >>> dt = datetime.datetime(2020, 5, 20, 8, 24, 0)
    >>> timeclock.time_ = dt
    >>> timeclock.save()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.timeclock_rounding = timedelta(minutes=30)
    >>> company.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.input_rounding_type = 'upward'
    >>> company.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.input_rounding_type = 'downward'
    >>> company.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.input_rounding_type = 'nearest'
    >>> company.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.input_rounding_type = 'no_rounding'
    >>> company.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)

Change rounded time::

    >>> timeclock.employee = employee2
    >>> timeclock.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)
    >>> company.input_rounding_type = 'downward'
    >>> company.save()
    >>> timeclock.employee = employee
    >>> timeclock.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 0)
    >>> company.input_rounding_type = 'nearest'
    >>> company.save()
    >>> timeclock.employee = employee2
    >>> timeclock.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 30)
    >>> company.input_rounding_type = 'no_rounding'
    >>> company.save()
    >>> timeclock.employee = employee
    >>> timeclock.save()
    >>> timeclock.reload()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 24)

Grace Period::

    >>> company.input_rounding_type = 'upward'
    >>> company.save()
    >>> company.grace_period = timedelta(minutes=5)
    >>> company.save()
    >>> timeclock.time_ = datetime.datetime(2020, 5, 20, 8, 4, 0)
    >>> timeclock.save()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 0)
    >>> timeclock.time_ = datetime.datetime(2020, 5, 20, 8, 5, 0)
    >>> timeclock.save()
    >>> timeclock.time_rounded
    datetime.datetime(2020, 5, 20, 8, 30)


Confirm second time clocking and try again::

    >>> timeclock.click('confirm')
	>>> party3 = Party(name='John Beesly')
	>>> party3.save()
	>>> employee3 = Employee(party=party3, company=company)
	>>> employee3.save()
	>>> employee3.clock_number
	'3'
	>>> timeclock2.employee = employee3
    >>> timeclock2.click('confirm')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Time clocking code "None" not defined. - 
    >>> timeclock2.code = 'IN'
    >>> timeclock2.save()
    >>> timeclock2.click('confirm')
	>>> period.click('close')
    >>> period.state
    'closed'

Check cron methods::
    
    >>> Cron = Model.get('ir.cron')
    >>> timeclock3 = TimeClock(clock_number='1')
    >>> timeclock3.time_ = now
    >>> timeclock3.save()
    >>> timeclock3.code == None
    True
    >>> cron = Cron(method='company.employee.time_clock|cron_set_codes')
    >>> cron.interval_number = 1
    >>> cron.interval_type = 'days'
    >>> cron.click('run_once')
    >>> timeclock3.reload()
    >>> timeclock3.code == 'IN'
    True

    >>> cron = Cron(method='company.employee.time_clock|cron_confirm')
    >>> cron.interval_number = 1
    >>> cron.interval_type = 'days'
    >>> timeclock3.save()
    >>> cron.click('run_once')
    >>> timeclock3.state
    'confirmed'
    >>> timeclock3.click('draft')
    >>> timeclock3.delete()

Add Work Hour to Employee::

	>>> EmployeeWork = Model.get('company.employee.work_hour')
	>>> emwork = EmployeeWork()
	>>> emwork.employee = employee
	>>> emwork.start_date = now + relativedelta(days=-5)
	>>> emwork.work_hour = workhour
	>>> emwork.save()


Start Time Clock User Edit Wizard::

    >>> config.user = user.id
    >>> len(TimeClock.find([]))
    2
    >>> Code = Model.get('company.time_clock.code')
    >>> code, = Code.find([('code', '=', 'IN')])
    >>> create_timeclock = Wizard('company.employee.time_clock.user_edit')
    >>> create_timeclock.form.employee == employee
    False
    >>> create_timeclock.form.time_ = now + relativedelta(days=-1)
    >>> create_timeclock.form.code = code
    >>> create_timeclock.execute('confirm_')
    >>> create_timeclock.form.password = 'abcde'
    >>> create_timeclock.execute('finish_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Invalid password! - 
    >>> create_timeclock.form.password = '12345678'
    >>> create_timeclock.execute('finish_')
    Traceback (most recent call last):
        ...
    AttributeError: 'NoneType' object has no attribute 'clock_number'


Check automatic code in Start Time Clock User Edit Wizard::


    >>> create_timeclock2 = Wizard('company.employee.time_clock.user_edit')
    >>> create_timeclock2.form.employee = employee
    >>> create_timeclock2.form.code = None
    >>> create_timeclock2.form.time_ = now + relativedelta(days=+4)
    >>> create_timeclock2.form.code.code
    'OUT'
    >>> create_timeclock2.execute('confirm_')
    >>> create_timeclock2.form.password = '12345678'
    >>> create_timeclock2.execute('finish_')

    >>> create_timeclock3 = Wizard('company.employee.time_clock.user_edit')
    >>> create_timeclock3.form.employee = employee
    >>> create_timeclock3.form.code = None
    >>> create_timeclock3.form.time_ = now + relativedelta(days=+5)
    >>> create_timeclock3.form.code.code
    'IN'
    >>> create_timeclock3.execute('confirm_')
    >>> create_timeclock3.form.password = '12345678'
    >>> create_timeclock3.execute('finish_')


Add employee to user::

    >>> config.user = admin.id
    >>> user.employees.append(employee)
    >>> user.employee = employee
    >>> user.save()


Start Time Clock User Edit Wizard again::

    >>> config.user = user.id
    >>> create_timeclock = Wizard('company.employee.time_clock.user_edit')
    >>> create_timeclock.form.employee == employee
    True
    >>> create_timeclock.form.time_ = now + relativedelta(days=-4)
    >>> create_timeclock.form.code = code
    >>> create_timeclock.execute('confirm_')
    >>> create_timeclock.form.password = '12345678'
    >>> create_timeclock.execute('finish_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...

Print report::

    >>> print_list = Wizard('company.employee.time_clock.print', [])
    >>> print_list.form.start_date = today + relativedelta(days=-5)
    >>> print_list.form.end_date = today
    >>> print_list.execute('pre_print')