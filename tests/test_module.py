# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.modules.company.tests import create_company
import datetime


class CompanyTimeClockTestCase(ModuleTestCase):
    """Test Company Time Clock module"""
    module = 'company_time_clock'
    extras = ['production_daily_end_time']

    @with_transaction()
    def test_party_clocking_number(self):
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Sequence = pool.get('ir.sequence')
        Conf = pool.get('party.configuration')
        ModelData = pool.get('ir.model.data')

        company1 = create_company()
        seq_id = ModelData.get_id('company_time_clock',
            'sequence_party_time_clock')
        seq = Sequence(seq_id)
        conf = Conf(1)
        conf.clocking_sequence = seq
        conf.save()

        party, = Party.create([{
                    'name': 'Pam Beesly',
                    }])
        self.assertTrue(not party.clock_number)

        employee, = Employee.create([{
                    'party': party.id,
                    'company': company1.id,
                    }])
        self.assertTrue(employee)
        self.assertTrue(employee.clock_number)

        employee2, = Employee.create([{
                    'party': party.id,
                    'company': company1.id,
                    }])
        self.assertEqual(employee.clock_number, employee2.clock_number)

    @with_transaction()
    def test_employee_workhour(self):
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        WorkHour = pool.get('company.work_hour')

        company1 = create_company()

        party, = Party.create([{
                    'name': 'Pam Beesly',
                    }])
        employee, = Employee.create([{
                    'party': party.id,
                    'company': company1.id,
                    }])

        wh1, wh2, wh3 = WorkHour.create([{
                'name': 'Workhour 1',
                'timezone': 'UTC'
            }, {
                'name': 'Workhour 2',
                'timezone': 'UTC'
            }, {
                'name': 'Workhour 3',
                'timezone': 'UTC'
            }])

        Employee.write([employee], {
            'work_hours': [('create', [{
                    'start_date': datetime.date(2019, 1, 1),
                    'work_hour': wh3
                }, {
                    'start_date': datetime.date(2019, 2, 12),
                    'work_hour': wh1
                }, {
                    'start_date': datetime.date(2019, 4, 1),
                    'work_hour': wh2

                }])]
            })
        values = [
            (datetime.date(2018, 12, 31), None),
            (datetime.date(2019, 1, 1), wh3),
            (datetime.date(2019, 1, 24), wh3),
            (datetime.date(2019, 2, 15), wh1),
            (datetime.date(2019, 4, 5), wh2),
            (datetime.date(2019, 12, 1), wh2),
        ]
        for date, work_hour in values:
            self.assertEqual(employee._get_work_hour(date), work_hour)


del ModuleTestCase
