datalife_company_time_clock
===========================

The company_time_clock module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-company_time_clock/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-company_time_clock)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
