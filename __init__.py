# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import timeclock
from . import employee
from . import work_hour
from . import production
from . import ir


def register():
    Pool.register(
        timeclock.TimeClockCode,
        timeclock.EmployeeTimeClock,
        timeclock.TimeClockPeriod,
        timeclock.TimeClockUserEditData,
        timeclock.TimeClockUserEditConfirm,
        timeclock.TimeClockPrintStart,
        employee.Employee,
        employee.Company,
        employee.Party,
        employee.PartyConfiguration,
        employee.ConfigurationSequence,
        work_hour.WorkHour,
        work_hour.WorkHourLine,
        work_hour.WorkHourCopyLineStart,
        work_hour.EmployeeWorkHour,
        work_hour.WorkHourGroup,
        work_hour.WorkHourGroupLine,
        work_hour.RotateWorkHour,
        ir.Cron,
        module='company_time_clock', type_='model')
    Pool.register(
        timeclock.TimeClockUserEdit,
        timeclock.TimeClockPrint,
        work_hour.WorkHourCopyLine,
        work_hour.EmployeeRotateWorkHour,
        module='company_time_clock', type_='wizard')
    Pool.register(
        timeclock.EmployeeTimeClockList,
        module='company_time_clock', type_='report')
    Pool.register(
        production.EmployeeTimeClockDailyEndTime,
        module='company_time_clock', type_='model',
        depends=['production_daily_end_time'])
    Pool.register(
        timeclock.TimeClockCodeDevice,
        timeclock.TimeClockCode_Device,
        timeclock.EmployeeTimeClockDevice,
        module='company_time_clock', type_='model',
        depends=['device'])
