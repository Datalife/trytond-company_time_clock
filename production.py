# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class EmployeeTimeClockDailyEndTime(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock'

    @fields.depends('time_tz')
    def on_change_with_date(self, name=None):
        if self.time_tz:
            pool = Pool()
            Config = pool.get('production.configuration')
            config = Config(1)
            return config.get_date_from_daily_end_time(self.time_tz)
