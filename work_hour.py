# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import pytz
from dateutil.tz import tz as dateutil_tz
from trytond.model import (ModelView, ModelSQL, fields,
    DeactivableMixin, sequence_ordered, Unique)
from trytond.pyson import Eval, Not, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.exceptions import UserError
from trytond.i18n import gettext
from datetime import timedelta, date, datetime

TIMEZONES = [(x, x) for x in pytz.common_timezones]


class WorkHour(ModelSQL, ModelView, DeactivableMixin):
    '''Work Hour'''
    __name__ = 'company.work_hour'

    _states = {
        'readonly': ~Eval('active')
    }

    name = fields.Char('Name', required=True, states=_states)
    timezone = fields.Selection(TIMEZONES, 'Timezone', translate=False,
        required=True, states=_states,
        help="Used to compute correctly the date times.")
    lines = fields.One2Many('company.work_hour.line', 'work_hour', 'Lines',
        readonly=True)
    monday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour', 'Monday Lines',
            domain=[('day', '=', '0')],
            context={'work_day': '0'}),
        'get_day_lines', setter='set_day_lines')
    tuesday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Tuesday Lines', domain=[('day', '=', '1')],
            context={'work_day': '1'}),
        'get_day_lines', setter='set_day_lines')
    wednesday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Wednesday Lines', domain=[('day', '=', '2')],
            context={'work_day': '2'}),
        'get_day_lines', setter='set_day_lines')
    thursday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Thursday Lines', domain=[('day', '=', '3')],
            context={'work_day': '3'}),
        'get_day_lines', setter='set_day_lines')
    friday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Friday Lines', domain=[('day', '=', '4')],
            context={'work_day': '4'}),
        'get_day_lines', setter='set_day_lines')
    saturday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Saturday Lines', domain=[('day', '=', '5')],
            context={'work_day': '5'}),
        'get_day_lines', setter='set_day_lines')
    sunday_lines = fields.Function(
        fields.One2Many('company.work_hour.line', 'work_hour',
            'Sunday Lines', domain=[('day', '=', '6')],
            context={'work_day': '6'}),
        'get_day_lines', setter='set_day_lines')

    del _states

    @classmethod
    def __setup__(cls):
        super(WorkHour, cls).__setup__()
        cls._buttons.update({
            'copy_lines': {
                'readonly': ~Eval('active'),
                'icon': 'tryton-copy',
                'depends': ['active']}
            })

    @staticmethod
    def default_active():
        return True

    @classmethod
    def set_day_lines(cls, records, name, value):
        if not value:
            return
        cls.write(records, {
                'lines': value,
                })

    def get_day_lines(self, name):
        lines = []
        day = self.get_day_dict()
        day_name = name.split('_')[0]
        for line in self.lines:
            if (line.day == day[day_name]):
                lines.append(line.id)
        return lines

    @classmethod
    def get_day_dict(cls, reverse=False):
        res = {}
        days = [
            ('monday', '0'),
            ('tuesday', '1'),
            ('wednesday', '2'),
            ('thursday', '3'),
            ('friday', '4'),
            ('saturday', '5'),
            ('sunday', '6')
        ]
        for item in days:
            res[item[1 if reverse else 0]] = item[0 if reverse else 1]
        return res

    @classmethod
    @ModelView.button_action('company_time_clock.wizard_copy_work_hour_line')
    def copy_lines(cls, records):
        pass

    def get_date_lines(self, at_date, tz_utc=False):
        day = str(at_date.weekday())
        days = self.get_day_dict(reverse=True)
        lines = getattr(self, '%s_lines' % days[day], [])
        values = [l.time_ for l in lines]
        if tz_utc and self.timezone != 'UTC':
            values = []
            lzone = dateutil_tz.gettz(self.timezone)
            szone = dateutil_tz.tzutc()
            for line in lines:
                value = datetime.combine(at_date, line.time_)
                value = value.replace(tzinfo=lzone).astimezone(
                    szone).replace(tzinfo=None)
                values.append(value.time())
        return values

    def get_hours(self, at_date):
        day = str(at_date.weekday())
        days = self.get_day_dict(reverse=True)
        lines = getattr(self, '%s_lines' % days[day], [])

        _inputs = [i for i in lines if i.type_ == 'in']
        _outputs = [i for i in lines if i.type_ == 'out']
        value = timedelta()
        for i, output in enumerate(_outputs):
            if len(_inputs) > i:
                output_time = datetime.combine(date.today(), output.time_)
                input_time = datetime.combine(date.today(),
                    _inputs[i].time_)
                value += (output_time - input_time)
        return value


class WorkHourLine(ModelSQL, ModelView):
    '''Work Hour Line'''
    __name__ = 'company.work_hour.line'

    work_hour = fields.Many2One('company.work_hour',
     'Work Hour', required=True, select=True, ondelete='CASCADE')
    day = fields.Selection([
            ('0', 'Monday'),
            ('1', 'Tuesday'),
            ('2', 'Wednesday'),
            ('3', 'Thursday'),
            ('4', 'Friday'),
            ('5', 'Saturday'),
            ('6', 'Sunday'),
            ], 'Day', required=True)
    type_ = fields.Selection([
            ('in', 'In'),
            ('out', 'Out')
            ], 'Type', required=True)
    time_ = fields.Time('Time', format='%H:%M:%S')

    @classmethod
    def __setup__(cls):
        super(WorkHourLine, cls).__setup__()
        cls._order.insert(0, ('day', 'ASC'))
        cls._order.insert(1, ('time_', 'ASC'))

    @classmethod
    def default_day(cls):
        return Transaction().context.get('work_day', None)


class WorkHourCopyLineStart(ModelView):
    '''Start copy line work hour'''
    __name__ = 'company.work_hour.copy_line.start'

    source = fields.Selection([
        ('0', 'Monday'),
        ('1', 'Tuesday'),
        ('2', 'Wednesday'),
        ('3', 'Thursday'),
        ('4', 'Friday'),
        ('5', 'Saturday'),
        ('6', 'Sunday'),
        ], 'Day', required=True, sort=False,
        help='Day which lines will be used as template.')
    monday = fields.Boolean('Monday',
        states={'readonly': (Eval('source') == '0')})
    tuesday = fields.Boolean('Tuesday',
        states={'readonly': (Eval('source') == '1')})
    wednesday = fields.Boolean('Wednesday',
        states={'readonly': (Eval('source') == '2')})
    thursday = fields.Boolean('Thursday',
        states={'readonly': (Eval('source') == '3')})
    friday = fields.Boolean('Friday',
        states={'readonly': (Eval('source') == '4')})
    saturday = fields.Boolean('Saturday',
        states={'readonly': (Eval('source') == '5')})
    sunday = fields.Boolean('Sunday',
        states={'readonly': (Eval('source') == '6')})

    @staticmethod
    def default_source():
        return '0'

    @fields.depends('source')
    def on_change_source(self):
        if self.source:
            Wh = Pool().get('company.work_hour')
            days = Wh.get_day_dict(reverse=True)
            setattr(self, days[self.source], False)


class WorkHourCopyLine(Wizard):
    '''Copy Line work hour'''
    __name__ = 'company.work_hour.copy_line'

    start = StateView('company.work_hour.copy_line.start',
        'company_time_clock.work_hour_copy_line_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'copy_', 'tryton-ok', default=True)])
    copy_ = StateTransition()

    def transition_copy_(self):
        pool = Pool()
        Wh = pool.get('company.work_hour')
        WhLine = pool.get('company.work_hour.line')

        wh_id = Transaction().context['active_id']

        days = Wh.get_day_dict()
        selected = []
        for day, value in days.items():
            if getattr(self.start, day, False) and value != self.start.source:
                selected.append(value)

        lines = WhLine.search([
            ('work_hour', '=', wh_id),
            ('day', 'in', selected)])
        if lines:
            WhLine.delete(lines)

        lines = WhLine.search([
            ('work_hour', '=', wh_id),
            ('day', '=', self.start.source)])
        for sel in selected:
            WhLine.copy(lines, default={'day': sel})
        return 'end'


class WorkHourGroup(ModelSQL, ModelView, DeactivableMixin):
    'WorkHourGroup'
    __name__ = 'company.work_hour.group'

    name = fields.Char('Name', required=True,
        states={'readonly': Not(Bool(Eval('active')))})
    lines = fields.One2Many('company.work_hour.group.line', 'group',
        'Lines', states={'readonly': Not(Bool(Eval('active')))})


class WorkHourGroupLine(sequence_ordered(), ModelSQL, ModelView):
    'WorkHourGroupLine'
    __name__ = 'company.work_hour.group.line'

    group = fields.Many2One('company.work_hour.group', 'Group', required=True,
        ondelete='CASCADE')
    work_hour = fields.Many2One('company.work_hour', 'Work Hour',
        required=True)
    period = fields.TimeDelta('Period')

    @fields.depends('period')
    def on_change_period(self):
        if self.period:
            days = self.period.total_seconds() // 86400
            self.period = timedelta(days=days)


class EmployeeWorkHour(ModelSQL, ModelView):
    '''Employee Work Hour'''
    __name__ = 'company.employee.work_hour'

    employee = fields.Many2One('company.employee', 'Employee',
        required=True, ondelete='CASCADE')
    start_date = fields.Date('Start Date', required=True)
    work_hour = fields.Many2One('company.work_hour', 'Work Hour',
        required=True, ondelete='RESTRICT')

    @classmethod
    def __setup__(cls):
        super(EmployeeWorkHour, cls).__setup__()
        w = cls.__table__()
        cls._sql_constraints += [
            ('employee_date_uk1', Unique(w, w.employee, w.start_date),
                'company_time_clock.msg_company_employee_work_hour_employee_date_uk1'),
        ]
        cls._order.insert(0, ('start_date', 'ASC'))

    @classmethod
    def validate(cls, records):
        super(EmployeeWorkHour, cls).validate(records)
        cls.check_validation(records)

    @classmethod
    def check_validation(cls, records):
        pool = Pool()
        Workhour = pool.get('company.work_hour')

        for record in records:
            if len(record.work_hour.lines) % 2:
                raise UserError(gettext(
                    'company_time_clock.msg_company_work_hour_line_paired',
                    work_hour=record.work_hour.rec_name))


class RotateWorkHour(ModelView):
    'Rotate Work Hour'
    __name__ = 'company.rotate_work_hour'

    start_date = fields.Date('Start date', required=True,
        domain=[('start_date', '<=', Eval('end_date'))])
    end_date = fields.Date('End date', required=True,
        domain=[('end_date', '>=', Eval('start_date'))])
    work_hour_group = fields.Many2One('company.work_hour.group',
        'Work Hours Group', required=True)


class EmployeeRotateWorkHour(Wizard):
    'Employee Rotate Work Hour'
    __name__ = 'company.employee.rotate_work_hour'
    start = StateView('company.rotate_work_hour',
        'company_time_clock.rotate_work_hour_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'rotate_', 'tryton-ok', default=True),
            ])
    rotate_ = StateTransition()

    def transition_rotate_(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        WorkHour = pool.get('company.employee.work_hour')
        employees = Employee.browse(Transaction().context['active_ids'])
        work_hour = []
        for employee in employees:
            if not self.start.work_hour_group.lines:
                continue
            date = self.start.start_date
            while date < self.start.end_date:
                for l in self.start.work_hour_group.lines:
                    if date < self.start.end_date:
                        work_hour.append({
                            'employee': employee.id,
                            'start_date': date,
                            'work_hour': l.work_hour.id
                            })
                    if l.period:
                        date += l.period
                    else:
                        date = self.start.end_date
        if work_hour:
            WorkHour.create(work_hour)
        return 'end'
