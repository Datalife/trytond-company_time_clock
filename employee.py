# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.model import ValueMixin
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Id
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
from datetime import timedelta


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    work_hours = fields.One2Many('company.employee.work_hour',
        'employee', 'Work Hours')
    work_hour = fields.Function(fields.Many2One('company.employee.work_hour',
        'Work Hour'), 'get_work_hour')
    clock_number = fields.Function(fields.Char('Clocking number'),
        'get_clock_number', searcher='search_clock_number')

    @classmethod
    def create(cls, vlist):
        party_ids = set()
        for vals in vlist:
            if vals.get('party'):
                party_ids.add(vals['party'])
        res = super(Employee, cls).create(vlist)
        cls._set_party_clocking_number(party_ids)
        return res

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []

        party_ids = set()
        for records, values in zip(actions, actions):
            if values.get('party', None):
                party_ids.add(values['party'])
            for record in records:
                if record.party:
                    party_ids.add(record.party.id)
            args.extend((records, values))
        super(Employee, cls).write(*args)
        cls._set_party_clocking_number(party_ids)

    def _get_work_hour(self, date):
        pool = Pool()
        EmployeeWorkHour = pool.get('company.employee.work_hour')

        work_hours = EmployeeWorkHour.search([
            ('employee', '=', self.id),
            ('start_date', '<=', date)
            ], order=[('start_date', 'DESC')])
        for wh in work_hours:
            if wh.start_date <= date:
                return wh.work_hour
        return None

    def get_work_hour(self, name):
        Date_ = Pool().get('ir.date')
        wh = self._get_work_hour(Date_.today())
        if wh:
            return wh.id
        return None

    @classmethod
    def _set_party_clocking_number(cls, party_ids):
        pool = Pool()
        Party = pool.get('party.party')

        if not party_ids:
            return
        parties = Party.search([
            ('id', 'in', [p for p in party_ids])])
        parties = [p for p in parties if not p.clock_number]
        if parties:
            Party._set_clocking_number(parties)

    @classmethod
    def get_clock_number(cls, records, name):
        return {r.id: r.party.clock_number for r in records}

    @classmethod
    def search_clock_number(cls, name, clause):
        return [('party.clock_number', ) + tuple(clause[1:])]

    @staticmethod
    def order_clock_number(tables):
        pool = Pool()
        Party = pool.get('party.party')
        line, _ = tables[None]
        if 'party' not in tables:
            party = Party.__table__()
            tables['party'] = {
                None: (party, line.party == party.id),
            }
        return Party.clock_number.convert_order('clock_number',
            tables['party'], Party)


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    clock_number = fields.Char('Clocking number', readonly=True)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['clock_number'] = None
        return super(Party, cls).copy(records, default=default)

    @classmethod
    def _set_clocking_number(cls, records):
        pool = Pool()
        Configuration = pool.get('party.configuration')

        config = Configuration(1)
        if not config.clocking_sequence:
            raise UserError(gettext(
                'company_time_clock.msg_party_configuration_missing_clocking_seq'))
        to_update = []
        for record in records:
            if record.clock_number:
                continue
            record.clock_number = config.get_multivalue(
                'clocking_sequence',
                company=Transaction().context.get('company', None)).get()
            to_update.append(record)
        if to_update:
            cls.save(to_update)


class PartyConfiguration(metaclass=PoolMeta):
    __name__ = 'party.configuration'

    clocking_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Clocking sequence',
            domain=[
                ('sequence_type', '=', Id('company_time_clock',
                    'sequence_type_party_time_clock'))
            ])
        )

    @classmethod
    def default_clocking_sequence(cls, **pattern):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('company_time_clock',
                'sequence_party_time_clock')
        except KeyError:
            return None


class ConfigurationSequence(ModelSQL, ValueMixin):
    '''Party Configuration Clocking Sequence'''
    __name__ = 'party.configuration.clocking_sequence'

    clocking_sequence = fields.Many2One('ir.sequence', 'Clocking sequence',
        domain=[
            ('sequence_type', '=', Id('company_time_clock',
                'sequence_type_party_time_clock'))
        ])


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    timeclock_rounding = fields.TimeDelta('Timeclock rounding',
        help='Gap for rounding time clocking')
    grace_period = fields.TimeDelta('Grace period',
        help='Gap for rounding late time clocking')
    input_rounding_type = fields.Selection([
        ('nearest', 'Nearest'),
        ('upward', 'Upward'),
        ('downward', 'Downward'),
        ('no_rounding', 'None')],
        'Input time rounding type', required=True)
    output_rounding_type = fields.Selection([
        ('nearest', 'Nearest'),
        ('upward', 'Upward'),
        ('downward', 'Downward'),
        ('no_rounding', 'None')],
        'Output time rounding type', required=True)

    @staticmethod
    def default_input_rounding_type():
        return 'nearest'

    @staticmethod
    def default_output_rounding_type():
        return 'nearest'

    @staticmethod
    def default_timeclock_rounding():
        return timedelta(minutes=0)
